<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model {
	public $from="explore@limadigit.com";
	public $fromName="Jukebox 5D";
	public function __construct(){
		parent::__construct();	
		$this->load->library('email');
		$this->load->model('Token');
		$this->load->helper('file');
	}
	public function register($data){
		$check=$this->checkEmail($data['email']);
		if($check){
			$status['status']=true;
			$this->db->insert('tbl_user',$data);
			$this->registerMail($data['email'],$data['status']);
		}else{
			$status['status']=false;
			$status['message']="Email is already exist, use another.";
		}
		return $status;
	}
	public function login($email,$password,$md5=true){
		$login=$this->Token->getToken($email,$password,$md5);
		if($login!=false){
			return $login;
		}else{
			$status['status']=false;
			$status['message']="Invalid Email and Password";
			return $status;
		}
	}
	public function getLocations($lat,$long,$tokens){
		$status=$this->Token->check_tokens($tokens);
		if($status['status']){
			$latlong = $lat."#".$long;
			$data_admin = array();
			$data_distance = array();
			$this->db->where('flag_login',1);
			$this->db->where('register_as','admin');
			$this->db->where('latlong!=','');
			$query=$this->db->get('tbl_user');
			foreach($query->result() as $d) {
			    $expVal = explode("#",$d->latlong);
			    $lat2 = $expVal[0];
			    $long2 = $expVal[1];
			    if($lat2 != "" && $long2 != "") {
			        $distance = round($this->distance($lat2,$long2,$lat,$long),2);
			        // echo $distance."====";
			        if($distance <= 2000) {
			            $data_distance[] = array(
			            	"location"=>$d->username,
			            	"distance"=>$distance,
			            	"user_id"=>$d->user_id
			            );
			        }
			    }
			}
			if(count($data_distance)>0){
				$status['status']=true;
				$status['data']=$data_distance;
			}else{
				$status['status']=false;
			}
			return $status;
		}else{
			return $status;
		}
	}
	public function changePassword($tokens,$newpassword){
		$status=$this->Token->check_tokens($tokens);
		if($status['status']){
			$data=array(
			'password'=>md5($newpassword)
			);
			$this->db->where('user_id',$status['user_id']);
			$this->db->update('tbl_user',$data);
			$status['status']=true;
			$status['message']="password has changed";	
			return $status;
		}else{
			return $status;
		}
	}
	public function uploadPhoto($photo,$id,$old_filename){
		//if($photo != "") {
			$new_file_name = 'android_' . $id . '_' . rand(0, 9999999999) . '.png';
			write_file('./assets/images/people/'.$new_file_name,$photo);
			$img = $destinationFolder.$new_file_name;
			$image_size_info 	= getimagesize($img); //get image size
			
			if($image_size_info){
				$image_width 		= $image_size_info[0]; //image width
				$image_height 	= $image_size_info[1]; //image height
				$image_type 		= $image_size_info['mime']; //image type
			}else{
				return false;
			}
			
			switch($image_type){
				case 'image/png':
					$image_res =  imagecreatefrompng($img); break;
				case 'image/gif':
					$image_res =  imagecreatefromgif($img); break;			
				case 'image/jpeg': case 'image/pjpeg':
					$image_res = imagecreatefromjpeg($img); break;
				default:
					$image_res = false;
			}

			if($image_res){
				//folder path to save resized images and thumbnails
				$thumb_save_folder 	= $destinationFolder . $new_file_name;

				if(!$this->crop_image_square($image_res, $thumb_save_folder, $image_type, $thumb_square_size, $image_width, $image_height, $jpeg_quality))
				{
					return false;
				}
				
				imagedestroy($image_res); //freeup memory
			}
			$this->db->where('user_id',$id);
			$this->db->update('tbl_user',array('twitter_image'=>'images/people/'.$new_file_name));
			if(!preg_match('/user\.jpg/',$R_username["twitter_image"])){
				unlink('./assets/images/people/'.$old_filename);
			}
			return true;
		//}
	}
	public function checkEmail($email){
		$this->db->where('email',$email);
		$query=$this->db->get('tbl_user');
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function checkUsername($username){
		$this->db->where('username',$username);
		$query=$this->db->get('tbl_user');
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function detailOthers($user_id,$token){
		$status=$this->Token->check_tokens($tokens);
		if($status['status']){
			$this->db->where('user_id',$user_id);
			$this->db->select('user_id,username,email,register_as,twitter_image,food,drink,hangout_place,music_genre,bio');
			$query=$this->db->get('tbl_user');
			if($query->num_rows()>0){
				$status['status']=true;
				$status['data']=$query->row();	
			}else{
				$status['status']=true;
				$status['message']="No Userdata Found";	
			}
			return $status;
		}else{
			return $status;
		}
	}
	public function editProfile($data,$token,$image){
		$status=$this->Token->check_tokens($tokens);
		if($status['status']){
			$this->db->where('user_id',$status['user_id']);
			$this->db->update('tbl_user',$data);
			if($image!=""){
				$this->uploadPhoto($image,$status['user_id'],$status['twitter_image']);
			}
			$status['status']=true;
			$status['message']="password has changed";	
			return $status;
		}else{
			return $status;
		}
	}
	public function registerMail($email,$statusUser){
		$emailsubject = "Email Confirmation for Jukebox-5D";		
		$content = "Halo Jukebox-ers<br/>
										Terimakasih sudah sign in dan masuk dalam salah satu komunitas musik di Indonesia.<br/>
										Dengan Anda terima email ini dan mengkonfirmasi maka Anda terdaftar sebagai ".$statusUser." Jukebox-5D. Lebih lanjut silahkan klik button konfirmasi email dibawah.<br/><br/>
										
										http://5dapps.com/music/new/confirmation.php?u=".$uniqueid."&f=".uniqid()."<br/><br/>
										
										Untuk mengetahui lebih lanjut perihal cara penggunaan Jukebox-5D silahkan klik: http://bit.ly/tutorial_jukebox-5d atau klik: limadigit.com/jukebox5d<br/>
										Follow twitter @jukebox5d atau instagram @jukebox5d<br/><br/>
										
										Sekali lagi terimakasih.<br/>
										Jangan lupa share ke komunitas Anda.<br/>
										Suara Anda sangat berharga untuk kemajuan musik.<br/><br/>

										Best Rgrds,<br/>
										Jukebox-5D Team<br/>
										limadigit.com";
		$this->sendMail($email,$emailsubject,$content);
	}
	public function forgotMail(){
		
	}
	public function sendMail($to,$subject,$message){
		$this->email->from($from, $fromName);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
	}
	public function detail($token){
		$this->db->where('token',$token);
		$detail=$this->db->get('tbl_tokens');
		if($detail->num_rows()>0){
			$this->db->where('user_id',$detail->row()->user_id);
			$this->db->select('user_id,username,email,register_as,twitter_image,food,drink,hangout_place,music_genre,bio');
			$user=$this->db->get('tbl_user');
			if($user->num_rows()>0){
				$status['status']=true;
				$status['data']=$user->row();			
			}else{
				$status['status']=false;
				$status['message']="No User data";
			}
		}else{
			$status['status']=false;
			$status['message']="Invalid Token";
		}
		return $status;
	}
	function crop_image_square($source, $destination, $image_type, $square_size, $image_width, $image_height, $quality){
		if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
		
		if( $image_width > $image_height )
		{
			$y_offset = 0;
			$x_offset = ($image_width - $image_height) / 2;
			$s_size 	= $image_width - ($x_offset * 2);
		}else{
			$x_offset = 0;
			$y_offset = ($image_height - $image_width) / 2;
			$s_size = $image_height - ($y_offset * 2);
		}
		$new_canvas	= imagecreatetruecolor($square_size, $square_size); //Create a new true color image
		
		//Copy and resize part of an image with resampling
		if(imagecopyresampled($new_canvas, $source, 0, 0, $x_offset, $y_offset, $square_size, $square_size, $s_size, $s_size)){
			save_image($new_canvas, $destination, $image_type, $quality);
		}

		return true;
	}

	##### Saves image resource to file ##### 
	function save_image($source, $destination, $image_type, $quality){
		switch(strtolower($image_type)){//determine mime type
			case 'image/png': 
				imagepng($source, $destination); return true; //save png file
				break;
			case 'image/gif': 
				imagegif($source, $destination); return true; //save gif file
				break;          
			case 'image/jpeg': case 'image/pjpeg': 
				imagejpeg($source, $destination, $quality); return true; //save jpeg file
				break;
			default: return false;
		}
	}
	function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000) {
	    // convert from degrees to radians
	    $latFrom = deg2rad($latitudeFrom);
	    $lonFrom = deg2rad($longitudeFrom);
	    $latTo = deg2rad($latitudeTo);
	    $lonTo = deg2rad($longitudeTo);

	    $lonDelta = $lonTo - $lonFrom;
	    $a = pow(cos($latTo) * sin($lonDelta), 2) + pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
	    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

	    $angle = atan2(sqrt($a), $b);
	    return $angle * $earthRadius;
	}
	public function SosmedLogin($username,$email,$image){
		$this->db->where('email',$email);
		$query=$this->db->get('tbl_user');
		// user register
		if($query->num_rows()>0){
			//login
			return $this->login($email,$query->row()->password,false);
		}else{
			$data=array(
				'username'=>$username,
				'email'=>$email,
				'password'=>md5(''),
				'twitter_image'=>$image,
				'flag_email'=>'1'
			);
			
			//register & login
			$this->db->insert('tbl_user',$data);
			return $this->login($email,'');
		}
	}
}
