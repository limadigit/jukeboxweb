<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct(){
		parent::__construct();	
		$this->load->model("Users");
		$this->load->library('form_validation');
	}
	public function index()
	{
		
	}
	public function change_password(){
		$this->form_validation->set_rules("token","Token","required");
		$this->form_validation->set_rules("password","Password","required");
		$this->form_validation->set_error_delimiters('','\n');
		if($this->form_validation->run()==TRUE){
			$password=$this->input->post("password");
			$token=$this->input->post("token");
			$status=$this->Users->changePassword($password);
		}else{
			$status['status']=false;
			$status['message']=validation_errors();
		}
		echo json_encode($status);
	}
	public function edit_profile(){
		$this->form_validation->set_rules("token","Token","required");
		$this->form_validation->set_rules("email","Email","required");
		$this->form_validation->set_rules("username","Username","required");
		$this->form_validation->set_error_delimiters('','\n');
		if($this->form_validation->run()==TRUE){
			$email=$this->input->post("email");
			$token=$this->input->post("token");
			$username=$this->input->post("username");
			$data=array(
				'email'=>$email,
				'username'=>$username
			);
			$photo=$this->input->post("photo");
			$status=$this->Users->editProfile($data,$token,$photo);
		}else{
			$status['status']=false;
			$status['message']=validation_errors();
		}
		echo json_encode($status);
	}
	public function profile(){
		$token=$_GET['token'];
		if(isset($token)){
			$status=$this->Users->detail($token);
		}else{
			$status['status']=false;
			$status['message']="Token is required";
		}
		echo json_encode($status);
	}
	public function detail(){
		$token=$_GET['token'];
		$user_id=$_GET['user_id'];
		if(isset($token)){
			$status=$this->Users->detailOthers($user_id,$token);
		}else{
			$status['status']=false;
			$status['message']="Token is required";
		}
		echo json_encode($status);
	}
	public function check_token(){
		$token=$_GET['token'];
		if(isset($token)){
			$status=$this->Token->check_tokens($token);
		}else{
			$status['status']=false;
			$status['message']="Token is required";
		}
		echo json_encode($status);
	}
	public function getsAdmin(){
		$lat = $_POST["lat"];
		$long = $_POST["long"];
		$token = $_POST["token"];
		$status=$this->Users->getLocations($lat,$long,$token);
		echo json_encode($status);
	}
	public function SosmedLogin(){
		$email = $_POST['email'];
		$username = $_POST['name'];
		$image = $_POST['image'];
		if($email==""){
			$status['message'] ="No Email Provided. Update Your Social Network Data !";
			$status['status'] = false;
		}else {
			$status=$this->Users->SosmedLogin($username,$email,$image);
		}
		echo json_encode($status);
	}
}
