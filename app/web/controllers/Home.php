<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("UserModel");
		$this->load->library('Mobile_Detect');
	}
	public function index()
	{
		$login=$this->UserModel->islogin();
		$loc=$this->session->userdata('user_id_admin');
		if($login){
			$data['user_logout']=site_url()."/login/logout";
			$data['username']=$this->getData('username');
			$admin=$this->getData('admin');
			$data['image']=$this->getData('image');
				if($admin) {
					$data['status']="admin";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id');
					$this->load->template('homeAdmin',$data);
				} else {
					$data['status']="user";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id_admin');
					$detect = new Mobile_Detect();
					if($loc!=""){
						if ( $detect->isMobile() ) {
			 				$data['user_home']=site_url();
			 				$this->load->template_mobile('mobile/homeUser',$data);
						} else {
							$this->load->template('homeUser',$data);				
						}
					}else{
						redirect(site_url().'/place');
					}
				}
		}else{
			redirect(site_url().'/login');
		}
	}
	public function getData($key){
		return $this->session->userdata('user_'.$key);
	}
	public function ManagePlaylist(){
		
	}
	
	public function WeeklyTrends(){
		
		$login=$this->UserModel->islogin();
		$loc=$this->session->userdata('user_id_admin');
		if($login){
			$data['user_logout']=site_url()."/login/logout";
			$data['username']=$this->getData('username');
			$admin=$this->getData('admin');
			if($loc!=""){
					
				if($admin){
					$data['status']="admin";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id');
				}else{
					$data['status']="user";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id_admin');
				}
				$data['image']=$this->getData('image');
				if($this->session->userdata("user_admin")) {
					$this->load->template('homeAdmin',$data);
				} else {
					$detect = new Mobile_Detect();
					if ( $detect->isMobile() ) {
		 				$data['user_home']=site_url();
		 				$this->load->template_mobile('mobile/weeklyTrends',$data);
					} else {
						$this->load->template('homeUser',$data);				
					}
				}
			}else{
				redirect(site_url().'/place');
			}
		}else{
			redirect(site_url().'/login');
		}
		
	}
	
	public function RecentSong(){
		
		$login=$this->UserModel->islogin();
		$loc=$this->session->userdata('user_id_admin');
		if($login){
			$data['user_logout']=site_url()."/login/logout";
			$data['username']=$this->getData('username');
			$admin=$this->getData('admin');
			if($loc!=""){
					
				if($admin){
					$data['status']="admin";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id');
				}else{
					$data['status']="user";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id_admin');
				}
				$data['image']=$this->getData('image');
				if($this->session->userdata("user_admin")) {
					$this->load->template('homeAdmin',$data);
				} else {
					$detect = new Mobile_Detect();
					if ( $detect->isMobile() ) {
		 				$data['user_home']=site_url();
		 				$this->load->template_mobile('mobile/recentSong',$data);
					} else {
						$this->load->template('homeUser',$data);				
					}
				}
			}else{
				redirect(site_url().'/place');
			}
		}else{
			redirect(site_url().'/login');
		}
		
	}
	
	public function MostRequest(){
		
		$login=$this->UserModel->islogin();
		$loc=$this->session->userdata('user_id_admin');
		if($login){
			$data['user_logout']=site_url()."/login/logout";
			$data['username']=$this->getData('username');
			$admin=$this->getData('admin');
			if($loc!=""){
					
				if($admin){
					$data['status']="admin";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id');
				}else{
					$data['status']="user";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id_admin');
				}
				$data['image']=$this->getData('image');
				if($this->session->userdata("user_admin")) {
					$this->load->template('homeAdmin',$data);
				} else {
					$detect = new Mobile_Detect();
					if ( $detect->isMobile() ) {
		 				$data['user_home']=site_url();
		 				$this->load->template_mobile('mobile/mostRequest',$data);
					} else {
						$this->load->template('homeUser',$data);				
					}
				}
			}else{
				redirect(site_url().'/place');
			}
		}else{
			redirect(site_url().'/login');
		}
		
	}
	
	public function ChatLounge(){
		
		$login=$this->UserModel->islogin();
		$loc=$this->session->userdata('user_id_admin');
		if($login){
			$data['user_logout']=site_url()."/login/logout";
			$data['username']=$this->getData('username');
			$admin=$this->getData('admin');
			if($loc!=""){
					
				if($admin){
					$data['status']="admin";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id');
				}else{
					$data['status']="user";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id_admin');
				}
				$data['image']=$this->getData('image');
				if($this->session->userdata("user_admin")) {
					$this->load->template('homeAdmin',$data);
				} else {
					$detect = new Mobile_Detect();
					if ( $detect->isMobile() ) {
		 				$data['user_home']=site_url();
		 				$this->load->template_mobile('mobile/chatLounge',$data);
					} else {
						$this->load->template('homeUser',$data);				
					}
				}
			}else{
				redirect(site_url().'/place');
			}
		}else{
			redirect(site_url().'/login');
		}
		
	}
	
	public function EditProfile(){
		
		$login=$this->UserModel->islogin();
		$loc=$this->session->userdata('user_id_admin');
		if($login){
			$data['user_logout']=site_url()."/login/logout";
			$data['username']=$this->getData('username');
			$data['email']=$this->getData('email');
			$data['music_genre']= (null !== $this->getData('music_genre')) ? json_decode($this->getData('music_genre')) : array();
			$data['bio']= (null !== $this->getData('bio')) ? $this->getData('bio') : "";
			$data['hangout_place']= (null !== $this->getData('hangout_place')) ? implode(',',json_decode($this->getData('hangout_place'))) : "";
			$data['food']= (null !== $this->getData('food')) ? implode(',',json_decode($this->getData('food'))) : "";
			$data['drink']= (null !== $this->getData('drink')) ? implode(',',json_decode($this->getData('drink'))) : "";
			$admin=$this->getData('admin');
			if($loc!=""){
					
				if($admin){
					$data['status']="admin";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id');
				}else{
					$data['status']="user";
					$data['id']=$this->getData('id');
					$data['loc_id']=$this->getData('id_admin');
				}
				$data['image']=$this->getData('image');
				if($this->session->userdata("user_admin")) {
					$this->load->template('homeAdmin',$data);
				} else {
					$detect = new Mobile_Detect();
					if ( $detect->isMobile() ) {
		 				$data['user_home']=site_url();
		 				$this->load->template_mobile('mobile/editProfile',$data);
					} else {
						$this->load->template('homeUser',$data);				
					}
				}
			}else{
				redirect(site_url().'/place');
			}
		}else{
			redirect(site_url().'/login');
		}
		
	}
	
}
