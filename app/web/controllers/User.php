<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("UserModel");
	}
	public function index()
	{
	
	}
	public function Profile(){
		$id=$_GET['id'];
		$detail=$this->UserModel->detail($id);
		if($detail){
			$status['status']=true;
			$status['data']=$detail;
		}else{
			$status['status']=false;
			$status['message']=$this->config->item("no_user_found");
		}
		echo json_encode($status);
	}
}
