<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->library("calculate");
		$this->load->library("email");
		$this->load->helper('file');
	}
	public function login($username,$password,$latitude,$longitude){
		$this->db->where('email',$username);
		$this->db->where('password',$password);
		$query=$this->db->get('tbl_user');
		$this->data = array();
		if($query->num_rows()>0){
			$this->setlogin(true);
			$user=$query->row();
			$this->setData('id',$user->user_id);
			$this->setData('username',$user->username);
			$this->setData('email',$user->email);
			$this->setData('image',$user->twitter_image);
			$this->setData('flag_email',$user->flag_email);
			$this->setData('bio',$user->bio);
			$this->setData('hangout_place',$user->hangout_place);
			// $this->setData('gender',$user->gender);
			$this->setData('food',$user->food);
			$this->setData('music_genre',$user->music_genre);
			$this->setData('drink',$user->drink);
			$this->setData('admin',false);
			if($user->register_as == "admin") {
				$this->setData('admin',true);
				$this->db->where("user_id",$user->user_id);
				$this->db->update('tbl_user',["last_login" => date("Y-m-d H:i:s"), "flag_login" => 1]);
				$this->data["status"] = true;
				$this->data["isAdmin"] = $this->getData('admin');
			} else {
				$this->db->where("flag_login",1);
				$this->db->where("register_as","admin");
				$query=$this->db->get('tbl_user');
				$this->data_distance = array();
				if($query->num_rows() > 0) {
					foreach($query->result_array() as $val) {
						$expVal = explode("#",$val["latlong"]);
						$latAdmin = $expVal[0];
						$longAdmin = $expVal[1];
						if($latitude !== "" && $longitude !== "") {
							$distance = round($this->calculate->distance($latAdmin,$longAdmin,$latitude,$longitude),2);
							if($distance <= 2000) {
								$this->data_distance[$val["location_name"]] = $distance . "#" . $val["user_id"] . "#" . $val["username"];
							}
						}
					}
					if(empty($this->data_distance)) {
						$this->setlogin(false);
						$this->data["status"] = false;
						$this->setData('all_location',array());
						$this->data["status_string"] = $this->config->item("no_server");
					} else {
						$this->setData('all_location',$this->data_distance);
						$this->data["status"] = true;
						$this->data["status_string"] = $this->config->item("success");
						$this->data["isAdmin"] = $this->getData('admin');
					}
				} else {
					$this->setlogin(false);
					$this->data["status"] = false;
					$this->data["status_string"] = $this->config->item("no_server");
				}
			}
		}else{
			$this->setlogin(false);
			$this->data["status"] = false;
			$this->data["status_string"] = $this->config->item("wrong_username_or_password");
		}
		// print_r($this->data);die;
		return $this->data;
	}
	
	public function updateLocation($location, $user_id) {
		$this->db->where("user_id",$user_id);
		$resultUpdate = $this->db->update("tbl_user",["location_name"=>$location]);
		return $resultUpdate;
	}
	
	public function islogin(){
		$data=$this->session->get_userdata();
		if(isset($data['is_login'])){
			return $data['is_login'];
		}else{
			return false;
		}
	}
	public function logout(){
		$this->session->sess_destroy();
	}
	public function setlogin($val){
		$this->session->set_userdata('is_login',$val);
	}
	public function setData($key,$val){
		$this->session->set_userdata('user_'.$key,$val);
	}
	public function getData($key){
		$data=$this->session->get_userdata();
		return $data['user_'.$key];
	}
	public function register($data){
		$data['verification_code']=md5(uniqid());
		$check=$this->checkEmail($data['email']);
		if($check){
			$status['status']=true;
			$this->db->insert('tbl_user',$data);
			if($data['register_as']=="admin"){
				write_file('./assets/chat/'.$this->db->insert_id().".txt", "");
			}
			$this->registerMail($data['email'],$data['register_as'],$data['verification_code']);
			
		}else{
			$status['status']=false;
			$status['message']= $this->config->item("email_already_exist");
		}
		return $status;
	}
	public function checkEmail($email){
		$this->db->where('email',$email);
		$query=$this->db->get('tbl_user');
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	}
	public function registerMail($email,$statusUser,$code){
		$emailsubject = $this->config->item('subjectEmailConfirmation');		
		$content =  $this->config->item('contentEmailConfirmation');
		$search=array(
			'[user]','[uniqueid]','[uniqid]'
		);
		$replace=array(
			$statusUser,$code,uniqid()
		);
		$content =str_replace($search,$replace,$content);
		$this->sendMail($email,$emailsubject,$content);
	}
	public function sendMail($to,$subject,$message){
		$this->email->from($this->config->item('from_email'), $this->config->item('from_email_name'));
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
	}
	public function updateData($data,$id){
		$username=$this->getData('username');
		$this->db->where('user_id',$id);
		$query=$this->db->update('tbl_user',$data);
		if($query){
			$this->db->query("UPDATE tbl_playlist set createby = '".$data['username']."' where createby = '".$username."'");
			$this->db->query("UPDATE tbl_top10player set name = '".$data['username']."' where name = '".$username."'");
			$this->setData('username',$data['username']);
			$this->setData('bio',$data['bio']);
			$this->setData('hangout_place',$data['hangout_place']);
			$this->setData('food',$data['food']);
			$this->setData('music_genre',$data['music_genre']);
			$this->setData('drink',$data['drink']);
			return true;
		}else{
			return false;
		}
	}
	public function SosmedLogin($username,$email,$image,$latitude,$longitude){
		$this->db->where('email',$email);
		$query=$this->db->get('tbl_user');
		// user register
		if($query->num_rows()>0){
			//login
			return $this->login($email,$query->row()->password, $latitude,$longitude);
		}else{
			$data=array(
				'username'=>$username,
				'email'=>$email,
				'twitter_image'=>$image,
				'flag_email'=>'1'
			);
			
			//register & login
			$this->db->insert('tbl_user',$data);
			return $this->login($email,'', $latitude,$longitude);
		}
	}
	public function updatePhoto($data){
		$this->db->trans_begin();
		$this->db->where('user_id',$this->session->userdata('user_id'));
		$this->db->update('tbl_user',$data);
		$this->db->where('createby',$this->session->userdata('user_username'));
		$this->db->update('tbl_playlist',$data);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	public function changePassword($oldpass,$newpassword,$retype,$id){
			$this->db->where('user_id',$id);
			$this->db->where('password',md5($oldpass));
			$query=$this->db->get('tbl_user');
			if($query->num_rows()>0){
				if($newpassword==$retype){
					$data=array(
						'password'=>md5($newpassword)
					);
					$this->db->where('user_id',$id);
					$this->db->update('tbl_user',$data);
					$status['status']=true;
					$status['message']= $this->config->item("success_change_pass");	
				}else{
					$status['status']=false;
					$status['message']= $this->config->item("password_not_match");
				}
			}else{
				$status['status']=false;
				$status['message']=$this->config->item("wrong_old_pass");
			}
			return $status;
	}
	public function detail($id){
		$this->db->where('user_id',$id);
		$this->db->select('username,music_genre,drink,twitter_image,bio,food,hangout_place');
		$get=$this->db->get('tbl_user');
		if($get->num_rows()>0){
			return $get->row();
		}else{
			return false;
		}
	}
}
