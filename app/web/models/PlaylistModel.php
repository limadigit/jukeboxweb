<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PlaylistModel extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	public function getPlaylistDaily($loc,$from,$to){
		$this->db->where('from_date<=',$from);
		$this->db->where('to_date>=',$to);
		$this->db->where('location',$loc);
		$query=$this->db->get('tbl_playlist_daily');
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function getPlaylistItems($playlist,$loc,$q){
		if(strtolower($q) != "all") {
			$this->db->like('title',$q);
		}
		$this->db->order_by('title','asc');
		$this->db->where('location',$loc);
		$this->db->where('playlist_name',$playlist);
		$query=$this->db->get('tbl_list_playlist_daily');
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return array();
		}
	}
	public function SearchPlaylist($q,$loc){
		$query=$this->getPlaylistDaily($loc,date('Y-m-d'),date('Y-m-d'));
		if($query){
			$data=array();
			foreach($query as $q){
				array_push($data,$this->getPlaylistItems($q->playlist_name,$loc,$q,$m));
			}
			if(count($data)>0){
				$data['length'] = count($data);
				$data['status']=true;
				return $data;
			}else{
				$data['status']=false;
				$data['err'] = $this->config->item("special_day_ajax");
				return $data;
			}
		}else{
			return false;
		}
	}
	public function getDislike($id){
		$this->db->where('playlist_id',$id);
		$query=$this->db->get('tbl_playlist');
		if($query->num_rows()>0){
			return $query->row()->playlist_id;
		}else{
			return array();
		}
	}
	public function insert($data){
		$query=$this->db->insert('tbl_playlist_daily',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	public function insertSong($data){
		$query=$this->db->insert('tbl_list_playlist_daily',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	public function delete($id){
		$this->db->where('playlist_daily_id',$id);
		$query=$this->db->delete('tbl_playlist_daily');
		if($query){
			return true;
		}else{
			return false;
		}
	}
	public function deleteSongPlaylist($id){
		$this->db->where('id_list_playlist_daily',$id);
		$query=$this->db->delete('tbl_list_playlist_daily');
		if($query){
			return true;
		}else{
			return false;
		}
	}
}
