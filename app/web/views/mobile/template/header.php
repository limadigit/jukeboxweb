<html>
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

    <title><?php echo $this->config->item("title") ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap/docs/examples/navbar-fixed-top/navbar-fixed-top.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap/docs/examples/sticky-footer/sticky-footer.css" rel="stylesheet">
	
	<?php if(isset($flag) && ($flag == "login" || $flag == "place")) { // ini buat template yg lama ?>
		<link href="<?php echo base_url(); ?>assets/css/bootstrap/bgstyle.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/bootstrap/docs/examples/signin/signin.css" rel="stylesheet">
		
	<?php } else if(isset($flag) && $flag == "register") { ?>
		<link href="<?php echo base_url(); ?>assets/css/bootstrap/docs/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/bootstrap/bgstyle.css" rel="stylesheet">
		
	
	<?php } else { ?>
		<link href="<?php echo base_url(); ?>assets/css/bootstrap/style-mobile.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/hideshare.css" rel="stylesheet">
	<?php } ?>
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:700' rel='stylesheet' type='text/css'>	
	<link href='https://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url(); ?>assets/css/bootstrap/sweetalert2.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap-social.css" rel="stylesheet">
	
	
	
	   
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/hideshare.js"></script>
	<script src="<?php echo base_url(); ?>assets/css/bootstrap/sweetalert2.min.js"></script>