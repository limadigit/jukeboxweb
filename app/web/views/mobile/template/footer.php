




<?php if(isset($flag) && $flag) { // ini buat template yg lama ?>
		
<?php } else { ?>
<div class="modal fade" id="detailUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="top:12%">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div id="loading" style="display:none;text-align: center;padding-top:20px;padding-bottom:25px;">
			<img src="<?php echo $this->config->item("ajax_loader"); ?>"/>
		</div>
		<div id="error" style="display:none;text-align: center;"></div>
		<div class="modal-body" id="bodyUserModel" style="display:none;">
			<a class="pull-right" data-dismiss="modal"><i class="fa fa-times"></i></a>
			<div class="row" style="background-color:#f5f5f5;padding:15px 0px 15px 0px">            
				<div class="col-xs-12 col-sm-4 text-center">
						<figure>
							<br/>
								<img id="userDetail-image" width="200px">						                       
						</figure>
				</div>
				<div class="col-xs-12 col-sm-8">
						<h2 id="userDetail-username"></h2>
						<p id="userDetail-bio"><strong>About: </strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
						
						<p id="userDetail-music-genre"><strong>Music Genre: </strong>
								<span class="tags">pop</span> 
								<span class="tags">jazz</span>
								<span class="tags">rock</span>
								<span class="tags">heavy metal</span>
						</p>
						<p id="userDetail-food"><strong>Food: </strong> <span>Ati Ampela, Orek, Usus</span></p>
						<p id="userDetail-drink"><strong>Drink: </strong> <span>Air putih</span></p>
						<p id="userDetail-hangout-place"><strong>Hangout Place: </strong> <span>Warteg</span></p>						                    
				</div>
			</div>
		</div>
		<!--<div class="modal-footer">
		
		</div>-->
	</div>
	</div>
</div>


<footer class="footer">
		  <div class="container">
			<span class="text-muted pull-left">&copy; Jukebox 5D  <?php echo $this->config->item("copyright_year") ?> | www.limadigit.com &nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="text-muted2 pull-right">Powered by: &nbsp;&nbsp;&nbsp;<img src='<?php echo base_url(); ?>assets/images/youtube.png' width="55px"/>&nbsp;&nbsp;&nbsp;<img src='<?php echo base_url(); ?>assets/images/soundcloud.png' width="40px"/></span>
		  </div>
		</footer>
		
<?php } ?>


<script src="<?php echo base_url(); ?>assets/css/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--<script src="<?php echo base_url(); ?>assets/css/bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>-->
<!--<script src="<?php echo base_url(); ?>assets/css/bootstrap/js/modal.js"></script>-->

</body>
</html>