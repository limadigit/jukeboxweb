<!--<script src="<?php echo base_url(); ?>assets/css/bootstrap/dist/js/bootstrap.min.js"></script>-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url(); ?>assets/css/bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
	<!--<script src="<?php echo base_url(); ?>assets/css/bootstrap/js/modal.js"></script>-->
<?php if(isset($flag) && $flag) { // ini buat template yg lama ?>
		
<?php } else { ?>
	<footer class="footer">
      <div class="container">
        <span class="text-muted">&copy; Jukebox 5D <?php echo $this->config->item("copyright_year") ?> | <a href="https://limadigit.com/" target="_blank" style="color:white">www.limadigit.com</a> &nbsp;&nbsp;&nbsp;&nbsp;</span> <span class="text-muted2"><a href="<?php echo $this->config->item("fb_jukebox5d") ?>" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a> &nbsp; <a href="<?php echo $this->config->item("twitter_jukebox5d") ?>" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>&nbsp;  <a href="<?php echo $this->config->item("ig_jukebox5d") ?>" target="_blank"><i class="fa fa-instagram fa-2x"></i></a></span>
		<span class="powered pull-right"><a style="cursor:pointer;color:white" id="howto" data-toggle="modal" data-target="#howtoModal" href="#" ><?php echo $this->config->item("ht") ?></a> &nbsp;&nbsp;|&nbsp;&nbsp; <a style="cursor:pointer;color:white" data-toggle="modal" data-target="#privacyModal" href="#"><?php echo $this->config->item("pp") ?></a> &nbsp;&nbsp;|&nbsp;&nbsp; Powered by : &nbsp;&nbsp;<img class="img-footer" src='<?php echo base_url(); ?>assets/images/youtube.png'/>&nbsp;&nbsp;&nbsp;<img class="img-footer" src='<?php echo base_url(); ?>assets/images/soundcloud.png'/></span>
      </div>
    </footer>
				
				<div class="modal fade" id="howtoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header" style="background-color:#00aeff;color:white">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel" style="font-weight:700"><i class="fa fa-info-circle"></i> <?php echo $this->config->item("how_to") ?></h4>
					  </div>
					  <div class="modal-body">
						<iframe src="<?php echo $this->config->item("video_tutorial"); ?>" width="100%" height="400" style="border:none"></iframe>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item("close") ?></button>						
					  </div>
					</div>
				  </div>
				</div>
				
				<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header" style="background-color:#00aeff;color:white">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel" style="font-weight:700"><i class="fa fa-black-tie"></i> <?php echo $this->config->item("pp") ?></h4>
					  </div>
					  <div class="modal-body">	
						<div class="row">
							<div class="col-sm-12">
								<center>
									<img src="<?php echo $this->config->item("logo5d2"); ?>" width="80px"/>
									<h2 class="form-signin-heading" style="margin-bottom:10px"><?php echo $this->config->item("privacy_policy"); ?></h2>
								</center>
									<br>
									<br>
								<?php echo $this->config->item("privacy_policy_body"); ?>
							</div>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item("close"); ?></button>						
					  </div>
					</div>
				  </div>
				</div>
			
					<div class="modal fade" id="detailUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
							<div id="loading" style="display:none;text-align: center;padding-top:20px;padding-bottom:25px;">
								<img src="<?php echo $this->config->item("ajax_loader"); ?>"/>
							</div>
							<div id="error" style="display:none;text-align: center;"></div>
						  <div class="modal-body" id="bodyUserModel" style="display:none;">
								<a class="pull-right" data-dismiss="modal"><i class="fa fa-times"></i></a>
								<div class="row" style="background-color:#f5f5f5;padding:15px 0px 15px 0px">            
									<div class="col-xs-12 col-sm-4 text-center">
											<figure>
												<br/>
													<img id="userDetail-image" width="80%">						                       
											</figure>
									</div>
									<div class="col-xs-12 col-sm-8">
											<h2 id="userDetail-username"></h2>
											<p id="userDetail-bio"><strong>About: </strong> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
											
											<p id="userDetail-music-genre"><strong>Music Genre: </strong>
													<span class="tags">pop</span> 
													<span class="tags">jazz</span>
													<span class="tags">rock</span>
													<span class="tags">heavy metal</span>
											</p>
											<p id="userDetail-food"><strong>Food: </strong> <span>Ati Ampela, Orek, Usus</span></p>
											<p id="userDetail-drink"><strong>Drink: </strong> <span>Air putih</span></p>
											<p id="userDetail-hangout-place"><strong>Hangout Place: </strong> <span>Warteg</span></p>						                    
									</div>
								</div>
						  </div>
						  <!--<div class="modal-footer">
							
						  </div>-->
						</div>
					  </div>
					</div>
<?php } ?>


</body>
</html>