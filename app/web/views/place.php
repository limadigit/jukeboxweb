<body>
    <div class="container">
		<div class="form-signin">
			<div class="col-lg-12 boxT">
				<center><img src="<?php echo $this->config->item("logo_login_jukebox"); ?>" width="100px" />
				<br/>
				<br/>
				
				<small style="font-family: 'Lato', sans-serif;font-size:10px;"><i><?php echo $this->config->item("subtitle") ?></i> </small></center>
				<br>
				<center><a target="_blank" href='<?php echo $this->config->item("play_store_link") ?>'><img alt='undefined' width="150px" src='<?php echo $this->config->item("google_play_logo"); ?>'/></a></center>
				<br>

					<div class="panel panel-default" id="boxLocation">
						<div class="panel-heading"><i class="fa fa-unlock-alt"></i> <?php echo $this->config->item("choose_admin"); ?></div>
							<div class="panel-body"  style="background-color:transparent;max-height:155px;overflow-y:auto">
							<?php 
							foreach($allLocation as $key=>$val) { 
							$expVal = explode("#",$val);
							?>
								<a onclick="getLocation('<?php echo $key; ?>','<?php echo $expVal[1]; ?>','<?php echo $expVal[2]; ?>')" style="cursor:pointer">
								<div class="col-xs-12" style="margin-bottom:15px;">
									<div class="col-xs-4" style="padding-right:0px" align="right">
										<i class="fa fa-map-marker fa-3x icon-color"></i>
									</div>
									<div class="col-xs-8" align="left">
										<label class="icon-color" style="font-size:14px;padding-top:2px;font-family: 'Lato', sans-serif;cursor:pointer;"><?php echo $key; ?> <br/> <small style="color:#888;font-size:10px"> <?php echo $expVal[0]; ?> M </small></label>
									</div>
								</div>
							</a>
							<?php 
							}
							?>
							</div>
						
					</div>
					
					<div id="loading" style="display:none;text-align: center;">
						<img src="<?php echo $this->config->item("ajax_loader"); ?>"/>						
					</div>
					
					<center id="cancelPlay"><small><?php echo $this->config->item("cancel_play"); ?> <a href="<?php echo site_url(); ?>/login/logout"><?php echo $this->config->item("sign_out"); ?></a></small></center>
			</div>
		</div>
    </div>
</body>

<script>
	function getLocation(loc,idAdmin, usernameAdmin) {
		$("#boxLocation").hide();
		$("#cancelPlay").hide();
		$("#loading").show();
		$.ajax({
			url:"<?php echo site_url(); ?>/place/updatelocation",
			type: "POST",
			data: {
				location: loc,
				ida: idAdmin,
				usernameAdmin: usernameAdmin
			},
			success: function(resp) {
				var result = JSON.parse(resp);
				if(result.status) {
					location.href = "<?php echo site_url(); ?>/home"
				} else {
					$("#error").html(result.message);
				}
			}
		});
	}
</script>